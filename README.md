[![pipeline status](https://gitlab.com/krakrjak/icfp2020-contest/badges/master/pipeline.svg)](https://gitlab.com/krakrjak/icfp2020-contest/-/commits/master)

Team Junk Food's solution to the ICFP 2020 Programming contest

https://icfpcontest2020.github.io

[[_TOC_]]

# Working With the Code

You need to install `stack` for your system, build the code, make changes and push a
branch. When you are happy with your branch, submit a PR to `master`. See [Submit] for
details on how to submit our solution.

## Get Stack

https://docs.haskellstack.org/en/stable/README/

## Build

If you've just installed `stack` or otherwise have a clean build tree then you need to
run `stack setup` one time. Once you've run `stack setup` you can proceed using `stack
build` to rebuild artifacts as you hack through the code.

If you get strange linking errors, make sure you have `libgmp` development libraries
installed for your system.

## Running

Once built your built binary will be in the `.stack-work/` directory tree. The easiest
way to find the directory is to use the `stack paths` helper. Here's an example that
executes the built binary:

```shell
    $(stack path --dist-dir)/build/main
```

You can also get the `main` binary "installed" in your home directory using `stack
install`. This will place the binary named `main` in `~/.local/bin`.

## Test (One Day)

If you've gotten a build you can run the, currently non-existent, test suite with `stack
test`.

## Benchmarking (One Day)

Perhaps one day we will support benchmarking... Once we do you will be able to run the
benchmarks by using `stack benchmark`.

## Submit

Submit an MR to the `submissions` branch.
