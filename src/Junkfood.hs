{-# language RecordWildCards #-}
module Junkfood (app) where

-- base
import Data.String (fromString)

-- bytestring
import qualified Data.ByteString.Char8 as CS

-- http-conduit
import Network.HTTP.Simple

-- optprase-applicative
import Options.Applicative (execParser)

-- text
import qualified Data.Text as Text

-- (this package)
import Junkfood.Cmdline
import Junkfood.Game
import Junkfood.Modem
import Junkfood.Play

mkSend :: String -> Maybe String -> IO (RadioData -> IO RadioData)
mkSend serverUrl mApiKey = do
  rawRequest <- parseRequest $ "POST " ++ serverUrl ++ pathQs
  let baseRequest = setRequestCheckStatus rawRequest
  return $ \rdin -> do
    respBS <- httpBS $ setRequestBody (fromString . signalBitString $ modulate rdin) baseRequest
    return . demodulate . bitStringSignal . CS.unpack $ getResponseBody respBS
 where
  basePath = "/aliens/send"
  pathQs =
    case mApiKey of
     Nothing -> basePath
     Just apiKey -> basePath ++ "?apiKey=" ++ apiKey

initialShipAttributes :: ShipAttributes
initialShipAttributes = SA
  { fuel = 128
  , saUnknown2 = 32
  , saUnknown3 = 12
  , saUnknown4 = 1
  }

app :: IO ()
app = do
  JFO {..} <- execParser jfParserInfo
  putStrLn ("ServerUrl: " ++ serverUrl ++ "; PlayerKey: " ++ show playerKey)
  send <- mkSend serverUrl apiKey
  let joinRecord = Join JR{ jrPlayerKey = playerKey, jrUnknown = [] }
  putStrLn $ "JOIN Request: " ++ show joinRecord
  rdJoinResp <- send $ printGameRequest joinRecord
  case scanGameResponse rdJoinResp of
   Left err -> putStrLn (Text.unpack err) >> putStrLn (rdShowPretty rdJoinResp)
   Right joinResp -> putStr "JOIN Response: " >> print joinResp
  let startRecord = Start SR{ srPlayerKey = playerKey, initialAttr = initialShipAttributes }
  putStrLn $ "START Request: " ++ show startRecord
  rdStartResp <- send $ printGameRequest startRecord
  case scanGameResponse rdStartResp of
    Left err -> do
      putStrLn $ rdShowPretty rdStartResp
      fail $ Text.unpack err
    Right BadRequest   -> fail "Bad Request"
    Right gr@(GR _ ge gs) -> do
      putStrLn "GameResponse: "
      print gr
      playTheGame PE { pePlayerKey = playerKey, sendAction = send, gameEnv = ge } gs

playTheGame :: PlayEnv -> Maybe GameState -> IO ()
playTheGame _                            Nothing = putStrLn "Can't play the game without a GameState, we are done here"
playTheGame env@(PE playerKey send _) (Just gs) = do
  let cmdRecord = Commands $ doNotCrash env gs
  putStrLn $ "Encoding command: " ++ show cmdRecord
  rdCommandsResp <- send $ printGameRequest cmdRecord
  putStrLn $ "Command Returned: " ++ rdShowPretty rdCommandsResp
  case scanGameResponse rdCommandsResp of
    Left err               -> fail $ Text.unpack err
    Right BadRequest       -> fail "Bad Request"
    Right (GR stage ge mgs) ->
      case mgs of
        Nothing -> playTheGame PE { pePlayerKey = playerKey, sendAction = send, gameEnv = ge } mgs
        Just g ->
          if stage >= 2
            then putStrLn $ "Game has entered completed stage after: " ++ show (gameTick g) ++ " ticks."
            else playTheGame PE { pePlayerKey = playerKey, sendAction = send, gameEnv = ge } mgs
