{-# language DefaultSignatures #-}
{-# language OverloadedStrings #-}
{-# language PatternSynonyms #-}
{-# language RecordWildCards #-}

module Junkfood.Game (
  GameRequest(..), JoinRequest(..), StartRequest(..), CommandsRequest(..), printGameRequest,
  ShipAttributes(..), scanShipAttributes,
  PlayerKey,
  Command(..), GameVector, accelerateCmd, detonateCmd, shootCmd, printCommand,
  CommandType, pattern Accelerate, pattern Detonate, pattern Shoot,
  RequestType, pattern CreateType, pattern JoinType, pattern StartType, pattern CommandsType,
  gameRequestType,
  GameResponse(..), gameResponseType, scanGameResponse,
  ResponseType, pattern BadRequestType, pattern Success,
  GameStage, pattern NotStarted, pattern InProgress, pattern Finished,
  GameRole, pattern Attacker, pattern Defender, GetRole(..), LensRole(..),
  GameEnv(..), GameState(..), ShipState(..),
  ShipId, GetShipId(..), LensShipId(..)
) where

-- base
import Data.Functor.Const (Const(Const), getConst)

-- text
import Data.Text (Text, pack)

import Junkfood.Modem

data GameRequest
  = Join !JoinRequest
  | Start !StartRequest
  | Commands !CommandsRequest
  deriving (Eq, Show, Read)

data JoinRequest = JR
  { jrPlayerKey :: PlayerKey
  , jrUnknown :: [RadioData]
  } deriving (Eq, Show, Read)

type PlayerKey = Integer

data StartRequest = SR
  { srPlayerKey :: PlayerKey
  , initialAttr :: ShipAttributes
  } deriving (Eq, Show, Read)

data ShipAttributes = SA
  { fuel :: Integer
  , saUnknown2 :: Integer
  , saUnknown3 :: Integer
  , saUnknown4 :: Integer
  } deriving (Eq, Show, Read)

printShipAttributes :: ShipAttributes -> RadioData
printShipAttributes SA{..} = RDList $ map RDI [fuel, saUnknown2, saUnknown3, saUnknown4]

scanShipAttributes :: RadioData -> Either Text ShipAttributes
scanShipAttributes (RDList [RDI f, RDI u2, RDI u3, RDI u4]) =
  Right SA{ fuel = f, saUnknown2 = u2, saUnknown3 = u3, saUnknown4 = u4 }
scanShipAttributes (RDList [RDI _, RDI _, RDI _, bad4]) =
  Left $ "scanShipAttributes: expected integer for unknown4, got: " <> pack (show bad4)
scanShipAttributes (RDList [RDI _, RDI _, bad3, _]) =
  Left $ "scanShipAttributes: expected integer for unknown3, got: " <> pack (show bad3)
scanShipAttributes (RDList [RDI _, bad2, _, _]) =
  Left $ "scanShipAttributes: expected integer for unknown2, got: " <> pack (show bad2)
scanShipAttributes (RDList [badF, _, _, _]) =
  Left $ "scanShipAttributes: expected integer for fuel, got: " <> pack (show badF)
scanShipAttributes (RDList l) =
  Left $ "scanShipAttributes: wrong length: " <> pack (show l)
scanShipAttributes rd =
  Left $ "scanShipAttributes: expected list, got: " <> pack (show rd)

data CommandsRequest = CR
  { crPlayerKey :: PlayerKey
  , commands :: [Command]
  } deriving (Eq, Show, Read)

data Command = Cmd
  { cmdType :: CommandType
  , cmdShipId :: ShipId
  , cmdParameters :: [RadioData]
  } deriving (Eq, Show, Read)

type ShipId = RadioData

type GameVector = (Integer, Integer)

type CommandType = Integer

pattern Accelerate :: CommandType
pattern Accelerate = 0

accelerateCmd :: ShipId -> GameVector -> Command
accelerateCmd si (x, y) =
  Cmd { cmdType = Accelerate, cmdShipId = si, cmdParameters = [RDPair (RDI x) (RDI y)] }

pattern Detonate :: CommandType
pattern Detonate = 1

detonateCmd :: ShipId -> Command
detonateCmd si = Cmd { cmdType = Detonate, cmdShipId = si, cmdParameters = [] }

pattern Shoot :: CommandType
pattern Shoot = 2

shootCmd :: ShipId -> GameVector -> RadioData -> Command
shootCmd si (x, y) unknown =
  Cmd { cmdType = Shoot, cmdShipId = si, cmdParameters = [RDPair (RDI x) (RDI y), unknown] }

printCommand :: Command -> RadioData
printCommand Cmd{..} = RDList $ [RDI cmdType, cmdShipId] ++ cmdParameters

scanCommand :: RadioData -> Either Text Command
scanCommand (RDList (rdty:si:params)) = do
  cty <- scanCommandType rdty
  return Cmd { cmdType = cty, cmdShipId = si, cmdParameters = params }
scanCommand (RDList l) = Left $ "scanCommand: wrong length: " <> pack (show l)
scanCommand rd = Left $ "scanCommand: expected list, got: " <> pack (show rd)

scanCommandType :: RadioData -> Either Text CommandType
scanCommandType (RDI cty) = Right cty
scanCommandType rd = Left $ "scanCommandType: expected integer, got: " <> pack (show rd)

type RequestType = Integer

pattern CreateType :: RequestType
pattern CreateType = 1

pattern JoinType :: RequestType
pattern JoinType = 2

pattern StartType :: RequestType
pattern StartType = 3

pattern CommandsType :: RequestType
pattern CommandsType = 4

gameRequestType :: GameRequest -> RequestType
gameRequestType Join{} = JoinType
gameRequestType Start{} = StartType
gameRequestType Commands{} = CommandsType

printGameRequest :: GameRequest -> RadioData
printGameRequest (Join JR{..}) = RDList [RDI JoinType, RDI jrPlayerKey, RDList jrUnknown]
printGameRequest (Start SR{..}) =
  RDList
    [ RDI StartType
    , RDI srPlayerKey
    , printShipAttributes initialAttr
    ]
printGameRequest (Commands CR{..}) =
  RDList [RDI CommandsType, RDI crPlayerKey, RDList $ map printCommand commands]

data GameResponse
  = BadRequest
  | GR GameStage GameEnv (Maybe GameState) -- GameState not sent at stage 0 and 2
  deriving (Eq, Show, Read)

type ResponseType = Integer

pattern BadRequestType :: ResponseType
pattern BadRequestType = 0

pattern Success :: ResponseType
pattern Success = 1

gameResponseType :: GameResponse -> ResponseType
gameResponseType BadRequest = BadRequestType
gameResponseType GR{} = Success

type GameStage = Integer

pattern NotStarted :: GameStage
pattern NotStarted = 0

pattern InProgress :: GameStage
pattern InProgress = 1

pattern Finished :: GameStage
pattern Finished = 2

data GameEnv = GE
  { maxTicks :: RadioData
  , geRole :: GameRole
  , geUnknown2 :: RadioData
  , geUnknown3 :: RadioData
  , geUnknown4 :: RadioData
  } deriving (Eq, Show, Read)

type GameRole = Integer

pattern Attacker :: GameRole
pattern Attacker = 0

pattern Defender :: GameRole
pattern Defender = 1

data GameState = GS
  { gameTick :: Integer
  , gsUnknown :: RadioData
  , shipsAndCommands :: [(ShipState, [Command])]
  } deriving (Eq, Show, Read)

data ShipState = SS
  { ssRole :: GameRole
  , ssShipId :: ShipId
  , position :: GameVector
  , velocity :: GameVector
  , attr :: ShipAttributes
  , ssUnknown5 :: RadioData
  , ssUnknown6 :: RadioData
  , ssUnknown7 :: RadioData
  } deriving (Eq, Show, Read)

scanGameResponse :: RadioData -> Either Text GameResponse
scanGameResponse (RDList []) = Left "scanGameResponse: no type provided"
scanGameResponse (RDList [RDI 0]) = Right BadRequest
scanGameResponse (RDList (RDI 0 : l)) =
  Left $ "scanGameResponse: wrong length for BadRequest type: " <> pack (show l)
scanGameResponse (RDList [RDI 1, gameStage, staticGameInfo, gameState]) =
  GR <$> scanGameStage gameStage <*> scanGameEnv staticGameInfo <*> pure mgs
 where
  mgs = either (const Nothing) Just $ scanGameState gameState
scanGameResponse (RDList (RDI 1 : l)) =
  Left $ "scanGameResponse: wrong length for Success type: " <> pack (show l)
scanGameResponse rd = Left $ "scanGameRespone: expected list, got: " <> pack (show rd)

scanGameStage :: RadioData -> Either Text GameStage
scanGameStage (RDI i) = Right i
scanGameStage rd = Left $ "scanGameStage: expecting integer, got: " <> pack (show rd)

scanGameEnv :: RadioData -> Either Text GameEnv
scanGameEnv (RDList [x0, rdr, x2, x3, x4]) = do
  r <- scanGameRole rdr
  return GE { maxTicks = x0, geRole = r, geUnknown2 = x2, geUnknown3 = x3, geUnknown4 = x4 }
scanGameEnv (RDList l) = Left $ "scanGameEnv: wrong length: " <> pack (show l)
scanGameEnv rd = Left $ "scanGameEnv: expected list, got: " <> pack (show rd)

scanGameRole :: RadioData -> Either Text GameRole
scanGameRole (RDI r) = Right r
scanGameRole rd = Left $ "scanGameRole: expected integer, got: " <> pack (show rd)

scanGameState :: RadioData -> Either Text GameState
scanGameState (RDList [RDI gt, x1, RDList sac]) = do
  sacGood <- traverse scanSAC sac
  return GS { gameTick = gt, gsUnknown = x1, shipsAndCommands = sacGood }
 where
  scanSAC (RDList [rdss, RDList rdcmds]) = do
    ss <- scanShipState rdss
    cmds <- traverse scanCommand rdcmds
    return (ss, cmds)
  scanSAC (RDList [_, cmds]) =
    Left $ "scanGameState: expected list for commands, got: " <> pack (show cmds)
  scanSAC (RDList l) =
    Left $ "scanGameState: wrong length for (ShipState, [Command]): " <> pack (show l)
  scanSAC rd =
    Left $ "scanGameState: expected list for (ShipState, [Command]), got: " <> pack (show rd)
scanGameState (RDList [_, _, sac]) =
  Left $ "scanGameState: expected list for shipsAndCommands, got: " <> pack (show sac)
scanGameState (RDList [gt, _, _]) =
  Left $ "scanGameState: expected integer gameTick, got: " <> pack (show gt)
scanGameState (RDList l) = Left $ "scanGameState: wrong length: " <> pack (show l)
scanGameState rd = Left $ "scanGameState: expected list, got: " <> pack (show rd)

scanShipState :: RadioData -> Either Text ShipState
scanShipState (RDList [rdr, si, rdpos, rdvel, rdatt, x5, x6, x7]) = do
  r <- scanGameRole rdr
  pos <- scanGameVector rdpos
  vel <- scanGameVector rdvel
  att <- scanShipAttributes rdatt
  return SS
    { ssRole = r
    , ssShipId = si
    , position = pos
    , velocity = vel
    , attr = att
    , ssUnknown5 = x5
    , ssUnknown6 = x6
    , ssUnknown7 = x7
    }
scanShipState (RDList [_, _, _, _, badAttr, _, _, _]) =
  Left $ "scanShipState: expected 4 element list for attributes, got: " <> pack (show badAttr)
scanShipState (RDList l) = Left $ "scanShipState: wrong length: " <> pack (show l)
scanShipState rd = Left $ "scanShipState: expected list, got: " <> pack (show rd)

scanGameVector :: RadioData -> Either Text GameVector
scanGameVector (RDPair (RDI x) (RDI y)) = Right (x, y)
scanGameVector (RDPair (RDI _) badY) =
  Left $ "scanGameVector: expected integer for second component, got: " <> pack (show badY)
scanGameVector (RDPair badX _) =
  Left $ "scanGameVector: expected integer for first component, got: " <> pack (show badX)
scanGameVector rd = Left $ "scanGameVector: expected pair, got: " <> pack (show rd)

class GetRole a where
  getRole :: a -> GameRole
  default getRole :: LensRole a => a -> GameRole
  getRole = getConst . role Const

class GetRole a => LensRole a where
  role :: Functor f => (GameRole -> f GameRole) -> a -> f a

instance LensRole ShipState where
  role inj ss = fmap (\newRole -> ss{ssRole = newRole}) (inj $ ssRole ss)
instance GetRole ShipState where

instance LensRole GameEnv where
  role inj ge = fmap (\newRole -> ge{geRole = newRole}) (inj $ geRole ge)
instance GetRole GameEnv where

class GetShipId a where
  getShipId :: a -> ShipId
  default getShipId :: LensShipId a => a -> ShipId
  getShipId = getConst . shipId Const

class GetShipId a => LensShipId a where
  shipId :: Functor f => (ShipId -> f ShipId) -> a -> f a

instance LensShipId Command where
  shipId inj cmd = fmap (\newShipId -> cmd{cmdShipId = newShipId}) (inj $ cmdShipId cmd)
instance GetShipId Command where
instance LensShipId ShipState where
  shipId inj ss = fmap (\newShipId -> ss{ssShipId = newShipId}) (inj $ ssShipId ss)
instance GetShipId ShipState where
