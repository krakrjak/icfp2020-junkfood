{-# language OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Junkfood.Untyped.Eval (evalMutual, esInitial, evalMutualMemo, exprValue) where

-- base
import Control.Monad (join)
import Data.Bits (unsafeShiftL)
import Data.Bool (bool)
import Prelude hiding (lookup)

-- containers
import Data.Map (Map, lookup, insert)
import qualified Data.Set as Set
import Data.Set (Set)

-- mtl
import Control.Monad.Except (MonadError, throwError)

-- text
import Data.Text (Text, pack)

-- transformers
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except (ExceptT, runExceptT, throwE)
import Control.Monad.Trans.Maybe (MaybeT(MaybeT), runMaybeT)
import Control.Monad.Trans.State (State, evalState, get, modify)

-- (this package)
import Junkfood.Modem
import Junkfood.Parse.Text.Type
import Junkfood.Untyped.Expr

{-|
@param initial global bindings
@param symbol to evaluate
@return failure message or value
-}
evalMutual :: Map Symbol Expr -> Expr -> Either Text Value
evalMutual gb x = do
  expr <- evalState (runExceptT $ evalMutualMemo x) $ esInitial gb
  case expr of
   Val v -> return v
   _ -> Left $ "eval not complete: " <> pack (show expr)

data EvalState = ES
  { esBindings :: Map Symbol Expr
  , esStack :: Set Symbol
  } deriving (Eq, Show, Read)

esInitial :: Map Symbol Expr -> EvalState
esInitial gb = ES { esBindings = gb, esStack = Set.empty }

{-|
@param initial global bindings
@param successful evaluations for global bindings
@param symbol to evaluate
@return failure message or updated verison of evaluations for global bindings, including the target
-}
evalMutualMemo :: Expr -> ExceptT Text (State EvalState) Expr
evalMutualMemo = go
 where
  goV :: Expr -> MaybeT (ExceptT Text (State EvalState)) Value
  goV expr = MaybeT $ do
    evald <- go expr
    return $ case evald of
     Val v -> Just v
     _ -> Nothing
  go expr = do
    case expr of
     Val _ -> return expr
     Sym name -> do
      ss <- lift get
      case lookup name $ esBindings ss of
       Nothing -> throwE $ "evalMutualMemo: Undefined Symbol: " <> pack (show name)
       Just bound ->
        if Set.member name $ esStack ss
         then return expr -- recursive hole, don't chase the dragon
         else do
          lift . modify $ \es -> es{esStack = Set.insert name $ esStack es}
          val <- go bound
          lift . modify $ \es -> es{esBindings = insert name val $ esBindings es, esStack = esStack ss}
          return val
     App ef ex -> do
      f <- go ef
      case f of
       Val vf -> do
        mreduce <- runMaybeT $ defaultApply vf ex
        case join mreduce of
         Nothing -> return . Val $ IrrApp vf ex
         Just reduce -> do
          mresult <- runMaybeT $ reduce goV
          case mresult of
           Nothing -> return . Val $ IrrApp vf ex
           Just result -> go result
       _ -> return expr

-- | NEEDS RENAME.  "Generic" evaluation loop.
exprValue :: Monad m => (Symbol -> m Expr) -> (Value -> Expr -> m (Maybe ((Expr -> m Value) -> m Expr))) -> Expr -> m Value
exprValue resolve apply = go
 where
  go expr = step expr >>= either return go
  step (Val v) = return $ Left v
  step (Sym s) = Right <$> resolve s
  step (App ef x) = do
    f <- go ef
    canReduce <- apply f x
    case canReduce of
     Nothing -> return . Left $ IrrApp f x
     Just reduce -> Right <$> reduce go

-- | How to apply a value to an argument.
defaultApply :: MonadError Text m => Value -> Expr -> m (Maybe ((Expr -> m Value) -> m Expr))
defaultApply (RDVal rd) x = Just . const . return <$> rdApply rd x
defaultApply (Prim p) x = defaultPrimApply1 p x
defaultApply (IrrApp (RDVal _) _) _ =
  throwError "defaultApply: Found redex in value: Applying radio data"
defaultApply (IrrApp (Prim p) x) y = defaultPrimApply2 p x y
defaultApply (IrrApp (IrrApp (RDVal _) _) _) _ =
  throwError "defaultApply: Found redex in value: Applying radio data"
defaultApply (IrrApp (IrrApp (Prim p) x) y) z = Just <$> defaultPrimApply3 p x y z
defaultApply (IrrApp (IrrApp (IrrApp _ _) _) _) _ =
  throwError "defaultApply: Found redex is vvalue: Applying to more than 3 arguments"

{-|
How to apply modem data.  Some modem data (Nil and Cons) can be interpeted as functions as
well.
-}
rdApply :: MonadError Text m => RadioData -> Expr -> m Expr
rdApply RDNil _ = return $ primitiveExpr T
rdApply (RDI _) _ = throwError "delayApply: Applying a number"
rdApply (RDPair f s) arg =
  return . App (App arg (radioDataExpr f)) $ radioDataExpr s

-- | Several primitive are strict, unary functions on the integers
intPrim1 :: MonadError Text m => (Integer -> Integer) -> Primitive -> Expr -> m (Maybe ((Expr -> m Value) -> m Expr))
intPrim1 f p x = return . Just $ \eval -> do
  v <- eval x
  case v of
   RDVal (RDI i) -> return . Lit $ f i
   _ -> throwError $ "intPrim1: Applying " <> pack (show p) <> " to a non-number"

-- | Helper for when none of the arguments need to be evaluated.
returnLazyEval :: Monad m => Expr -> m ((Expr -> m Value) -> m Expr)
returnLazyEval x = return . const $ return x

-- | Helper for when none of the arguments need to be evaluated, but other cases can fail to apply.
returnMaybeLazyEval :: Monad m => Expr -> m (Maybe ((Expr -> m Value) -> m Expr))
returnMaybeLazyEval = fmap Just . returnLazyEval

{-|
How to apply a primitive to one argument.  Some primitives need more arguments, and will
return Nothing to indicate that.
-}
defaultPrimApply1 :: MonadError Text m => Primitive -> Expr -> m (Maybe ((Expr -> m Value) -> m Expr))
-- 1 argument
defaultPrimApply1 Car x = returnMaybeLazyEval $ App x (primitiveExpr T)
defaultPrimApply1 Cdr x = returnMaybeLazyEval $ App x (primitiveExpr F)
defaultPrimApply1 Dec x = intPrim1 (subtract 1) Dec x
defaultPrimApply1 I x = returnMaybeLazyEval x
defaultPrimApply1 Inc x = intPrim1 (+1) Inc x
defaultPrimApply1 IsNil x = return . Just $ \eval -> do
  v <- eval x
  case v of
   RDVal RDNil -> return $ primitiveExpr T
   RDVal (RDPair _ _) -> return $ primitiveExpr F
   Prim Nil -> return $ primitiveExpr T
   IrrApp (IrrApp (Prim Cons) _) _ -> return $ primitiveExpr F
   _ -> throwError "defaultPrimApply1: Bad argument to IsNil"
defaultPrimApply1 Neg x = intPrim1 negate Neg x
defaultPrimApply1 Nil _ = returnMaybeLazyEval $ primitiveExpr T
defaultPrimApply1 Pow2 x = intPrim1 (unsafeShiftL 1 . fromInteger) Pow2 x
-- 2 arguments
defaultPrimApply1 Add _ = return Nothing
defaultPrimApply1 Div _ = return Nothing
defaultPrimApply1 Eq _ = return Nothing
defaultPrimApply1 F _ = return Nothing
defaultPrimApply1 Lt _ = return Nothing
defaultPrimApply1 Mul _ = return Nothing
defaultPrimApply1 T _ = return Nothing
-- 3 arguments
defaultPrimApply1 B _ = return Nothing
defaultPrimApply1 C _ = return Nothing
defaultPrimApply1 Cons _ = return Nothing
defaultPrimApply1 S _ = return Nothing
defaultPrimApply1 If0 _ = return Nothing

{-|
When a primitive is _over_ saturated, it means an IrrApp was used instead of an App,
incorrectly.
-}
oversaturatedPrimError :: MonadError Text m => Primitive -> m a
oversaturatedPrimError p =
  throwError $ "oversaturatedPrimError: found redex in value: primitive: " <> pack (show p)

-- | Primitives that are strict, binary operations from and to the Integers
intPrim2
  :: MonadError Text m
  => (Integer -> Integer -> Integer)
  -> Primitive
  -> Expr
  -> Expr
  -> m (Maybe ((Expr -> m Value) -> m Expr))
intPrim2 f p x y = return . Just $ \eval -> do
  vi <- eval x
  case vi of
   RDVal (RDI i) -> do
    vj <- eval y
    case vj of
     RDVal (RDI j) -> return . Val . RDVal . RDI $ f i j
     _ -> throwError $ "intPrim2: Applying " <> pack (show p) <> " to a non-number (second argument)"
   _ -> throwError $ "intPrim2: Applying " <> pack (show p) <> " to a non-number (first argument)"

-- | Primitives that are binary relations on the Integers
intRelPrim2
  :: MonadError Text m
  => (Integer -> Integer -> Bool)
  -> Primitive
  -> Expr
  -> Expr
  -> m (Maybe ((Expr -> m Value) -> m Expr))
intRelPrim2 f p x y = return . Just $ \eval -> do
  vi <- eval x
  case vi of
   RDVal (RDI i) -> do
    vj <- eval y
    case vj of
     RDVal (RDI j) -> return . primitiveExpr . bool F T $ f i j
     _ -> throwError $ "intPrim2: Applying " <> pack (show p) <> " to a non-number (second argument)"
   _ -> throwError $ "intPrim2: Applying " <> pack (show p) <> " to a non-number (first argument)"

-- | How to apply primitives to two arguments.  Nothing in the maybe means we need more arguments.
defaultPrimApply2 :: MonadError Text m => Primitive -> Expr -> Expr -> m (Maybe ((Expr -> m Value) -> m Expr))
-- 1 argument
defaultPrimApply2 Car _ _ = oversaturatedPrimError Car
defaultPrimApply2 Cdr _ _ = oversaturatedPrimError Cdr
defaultPrimApply2 Dec _ _ = oversaturatedPrimError Dec
defaultPrimApply2 I _ _ = oversaturatedPrimError I
defaultPrimApply2 Inc _ _ = oversaturatedPrimError Inc
defaultPrimApply2 IsNil _ _ = oversaturatedPrimError IsNil
defaultPrimApply2 Neg _ _ = oversaturatedPrimError Neg
defaultPrimApply2 Nil _ _ = oversaturatedPrimError Nil
defaultPrimApply2 Pow2 _ _ = oversaturatedPrimError Pow2
-- 2 arguments
defaultPrimApply2 Add x y = intPrim2 (+) Add x y
defaultPrimApply2 Div x y = intPrim2 quot Div x y
defaultPrimApply2 Eq x y = intRelPrim2 (==) Eq x y
defaultPrimApply2 F _ y = returnMaybeLazyEval y
defaultPrimApply2 Lt x y = intRelPrim2 (<) Lt x y
defaultPrimApply2 Mul x y = intPrim2 (*) Mul x y
defaultPrimApply2 T x _ = returnMaybeLazyEval x
-- 3 arguments
defaultPrimApply2 B _ _ = return Nothing
defaultPrimApply2 C _ _ = return Nothing
defaultPrimApply2 Cons x y = return . Just $ \eval -> do
  vx <- eval x
  vy <- eval y
  case (vx, vy) of
   (RDVal f, RDVal s) -> return . Val . RDVal $ RDPair f s
   _ -> return . Val $ IrrApp (IrrApp (Prim Cons) (Val vx)) (Val vy)
defaultPrimApply2 S _ _ = return Nothing
defaultPrimApply2 If0 _ _ = return Nothing

-- | How to apply a primitive to 3 arguments.
defaultPrimApply3 :: MonadError Text m => Primitive -> Expr -> Expr -> Expr -> m ((Expr -> m Value) -> m Expr)
-- 1 argument
defaultPrimApply3 Car _ _ _ = oversaturatedPrimError Car
defaultPrimApply3 Cdr _ _ _ = oversaturatedPrimError Cdr
defaultPrimApply3 Dec _ _ _ = oversaturatedPrimError Dec
defaultPrimApply3 I _ _ _ = oversaturatedPrimError I
defaultPrimApply3 Inc _ _ _ = oversaturatedPrimError Inc
defaultPrimApply3 IsNil _ _ _ = oversaturatedPrimError IsNil
defaultPrimApply3 Neg _ _ _ = oversaturatedPrimError Neg
defaultPrimApply3 Nil _ _ _ = oversaturatedPrimError Nil
defaultPrimApply3 Pow2 _ _ _ = oversaturatedPrimError Pow2
-- 2 arguments
defaultPrimApply3 Add _ _ _ = oversaturatedPrimError Add
defaultPrimApply3 Div _ _ _ = oversaturatedPrimError Div
defaultPrimApply3 Eq _ _ _ = oversaturatedPrimError Eq
defaultPrimApply3 F _ _ _ = oversaturatedPrimError F
defaultPrimApply3 Lt _ _ _ = oversaturatedPrimError Lt
defaultPrimApply3 Mul _ _ _ = oversaturatedPrimError Mul
defaultPrimApply3 T _ _ _ = oversaturatedPrimError T
-- 3 arguments
defaultPrimApply3 B x y z = returnLazyEval $ App x (App y z)
defaultPrimApply3 C x y z = returnLazyEval $ App (App x z) y
defaultPrimApply3 Cons x y z = returnLazyEval $ App (App z x) y
defaultPrimApply3 S x y z = returnLazyEval $ App (App x z) (App y z)
defaultPrimApply3 If0 x y z = return $ \eval -> do
  vi <- eval x
  case vi of
   RDVal (RDI 0) -> return y
   RDVal (RDI 1) -> return z
   _ -> throwError $ "defaultPrimApply3: Applying If0 to not 0 or 1: " <> pack (show vi)
