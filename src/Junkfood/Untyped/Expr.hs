{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}

module Junkfood.Untyped.Expr where

-- base
import Control.Monad
import GHC.Generics

-- aesom
import Data.Aeson (ToJSON(toEncoding), genericToEncoding, defaultOptions, FromJSON)

-- (this package)
import Junkfood.Modem
import Junkfood.Parse.Text.Type

data Primitive
  = Add
  | B
  | C
  | Car
  | Cdr
  | Cons
  | Dec
  | Div
  | Eq
  | F
  | I
  | If0
  | Inc
  | IsNil
  | Lt
  | Mul
  | Neg
  | Nil
  | Pow2
  | S
  | T
  deriving (Eq, Ord, Read, Show, Generic)

instance ToJSON Primitive where
instance FromJSON Primitive where

data Expr
  = Val !Value
  | App_ Expr Expr
  | Sym !Symbol
  deriving (Eq, Read, Show, Generic)

pattern App :: Expr -> Expr -> Expr
pattern App f x <- (exprApplication -> Just (f, x))
 where
  App (Val vf@(Prim Cons)) x = Val $ IrrApp vf x
  App (App (Val (Prim Cons)) (Val (RDVal f))) (Val (RDVal s)) = Val . RDVal $ RDPair f s
  App f x = App_ f x

exprApplication :: Expr -> Maybe (Expr, Expr)
exprApplication (Val (IrrApp vf x)) = Just (Val vf, x)
exprApplication (App_ f x) = Just (f, x)
exprApplication _ = Nothing

{-# COMPLETE Val, App, Sym #-}

instance ToJSON Expr where toEncoding = genericToEncoding defaultOptions
instance FromJSON Expr

data Value
  = RDVal !RadioData
  | Prim_ !Primitive
  | IrrApp !Value !Expr
  deriving (Eq, Read, Show, Generic)

instance ToJSON Value where
instance FromJSON Value where

pattern Prim :: Primitive -> Value
pattern Prim p <- (valuePrimitive -> Just p)
 where
  Prim Nil = RDVal RDNil
  Prim p = Prim_ p

{-# COMPLETE RDVal, Prim, IrrApp #-}

pattern Lit  :: Integer -> Expr
pattern Lit i = Val (RDVal (RDI i))

valuePrimitive :: Value -> Maybe Primitive
valuePrimitive (RDVal RDNil) = Just Nil
valuePrimitive (Prim_ p) = Just p
valuePrimitive _ = Nothing

primitiveExpr :: Primitive -> Expr
primitiveExpr = Val . Prim

radioDataExpr :: RadioData -> Expr
radioDataExpr = Val . RDVal

-- | Used to opporuntistically apply Cons, if arguments are simple.
exprRadioData :: Expr -> Maybe RadioData
exprRadioData (Val (RDVal rd)) = return rd
exprRadioData (Val (Prim Nil)) = return RDNil
exprRadioData (Val (Prim _)) = Nothing
exprRadioData (Val (IrrApp (IrrApp (Prim Cons) f) s)) =
  RDPair <$> exprRadioData f <*> exprRadioData s
exprRadioData (Val (IrrApp _ _)) = Nothing
exprRadioData (Lit i) = return $ RDI i
exprRadioData (Sym _) = Nothing
exprRadioData (App (App (Val (Prim Cons)) f) s) = RDPair <$> exprRadioData f <*> exprRadioData s
exprRadioData (App _ _) = Nothing

lispExprToExpr :: LispExpr -> Either String Expr
lispExprToExpr (Ap x y) = liftM2 App (lispExprToExpr x) (lispExprToExpr y)
lispExprToExpr (Tok tok') =
  case tok' of
    TLit x -> return $ Lit x
    TSym x -> return $ Sym x
    TVar x -> Val . Prim <$>
      case x of
        "add" -> return Add
        "ap" -> Left "lispExprToExpr: \"ap\" has a dedicated constructuor, not a Var"
        "b" -> return B
        "c" -> return C
        "car" -> return Car
        "cdr" -> return Cdr
        "cons" -> return Cons
        "div" -> return Div
        "eq" -> return Eq
        "f" -> return F
        "i" -> return I
        "isnil" -> return IsNil
        "lt" -> return Lt
        "mul" -> return Mul
        "neg" -> return Neg
        "nil" -> return Nil
        "s" -> return S
        "t" -> return T
        _ -> Left $ "lispExprToExpr: unknown Var: " ++ show x
