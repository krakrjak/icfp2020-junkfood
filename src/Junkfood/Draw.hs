module Junkfood.Draw (draw, multipleDraw, Picture, printPicture) where

-- base
import Data.Bool (bool)

-- (this package)
import Junkfood.Game

type Picture = [String]

fullBlock :: Char
fullBlock = '\x2588'

draw :: [GameVector] -> Picture
draw v = map (\y -> map (\x -> drawChar $ elem (x, y) vo) [0..xn]) [0..yn]
 where
  drawChar = bool ' ' fullBlock
  (x0, y0) = foldr (\(x, y) (x_, y_) -> (min x x_, min y y_)) (0, 0) v
  (mx, my, vo) =
    foldr
      (\(x, y) (xm, ym, t) -> (max x xm, max y ym, (x - x0, y - y0) : t))
      (x0 + 32, y0 + 16, [])
      v
  xn = mx - x0
  yn = my - y0

multipleDraw :: [[GameVector]] -> [Picture]
multipleDraw = map draw

printPicture :: Picture -> IO ()
printPicture [] = return ()
printPicture l@(h:_) = do
  hBorder
  mapM_ (\row -> putChar fullBlock >> putStr row >> putChar fullBlock >> putChar '\n') l
  hBorder
 where
  hBorder = putChar ' ' >> mapM_ (const $ putChar fullBlock) h >> putChar ' ' >> putChar '\n'
