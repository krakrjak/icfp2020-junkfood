{-# language RecordWildCards #-}
module Junkfood.Play (PlayEnv(..), doNotCrash) where

-- base
import Data.Bifunctor (bimap)
import Data.Bits (Bits, unsafeShiftL)
import Data.Bool (bool)
import Data.List (find)
import Data.Maybe (fromJust, mapMaybe)

-- (this package)
import Junkfood.Game
import Junkfood.Modem

data PlayEnv = PE
  { pePlayerKey :: PlayerKey
  , sendAction :: RadioData -> IO RadioData
  , gameEnv :: GameEnv
  }
{-|
  Just don't crash into the planet please
 -}
doNotCrash :: PlayEnv -> GameState -> CommandsRequest
doNotCrash (PE playerKey _ (GE _ myRole _ _ _)) (GS _ _ scs) = CR
  { crPlayerKey = playerKey
  , commands = thrustCommands myRole shipStates ++ shoot
  }
 where
  -- Potential Partial function, for this contest our ship will always be in the list... or else
  myState :: (ShipState, [Command])
  myState = fromJust . find (\s -> myRole == getRole (fst s)) $ scs
  myShipState :: ShipState
  myShipState = fst myState
  myShipId :: ShipId
  myShipId = ssShipId myShipState
  shipStates = map fst scs
  shoot :: [Command]
  shoot =
    case shootProneShip myRole shipStates of
      Nothing -> []
      Just (x, y) -> [shootCmd myShipId (x, y) (RDI 32)]

isProne :: ShipState -> Bool
isProne s = 0 == fuel (attr s)

firstProneShip :: GameRole -> [ShipState] -> Maybe ShipState
firstProneShip tgtRole = find (\s -> (tgtRole == ssRole s) && isProne s)

shootProneShip :: GameRole -> [ShipState] -> Maybe GameVector
shootProneShip grole ss =
  case firstProneShip grole ss of
    Nothing                   -> Nothing
    Just (SS _ _ p v _ _ _ _) -> Just (vectorAdd (gravityEstimate $ vectorAdd p v) $ vectorAdd p v)

vectorAdd :: GameVector -> GameVector -> GameVector
vectorAdd (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

clamp :: Ord a => a -> a -> a -> a
clamp mn mx val = max mn (min val mx)

(.<<.) :: Bits a => a -> Int -> a
(.<<.) = unsafeShiftL

gravityEstimate :: GameVector -> GameVector
gravityEstimate (x, y) =
  case (y2x, xn2y) of
   (False, False) -> bool (0, 1) (-1, 1) yn2x
   (False, True) -> bool (-1, 0) (-1, -1) x2y
   (True, False) -> bool (1, 1) (1, 0) x2y
   (True, True) -> bool (1, -1) (0, -1) yn2x
 where
  y2x = y > (x .<<. 1) -- y > 2x
  yn2x = y > negate (x .<<. 1) -- y > -2x
  x2y = (y .<<. 1) > x -- 2y > x
  xn2y = negate (y .<<. 1) < x -- -2y < x

targetVelocity :: GameVector -> GameVector
targetVelocity gravEst =
  case gravEst of
   (0, i) -> (3 * i, 0)
   (i, 0) -> (0, -3 * i)
   (i, j) -> (2 * j, -2 * i)

-- | First argument is expected velocity; second argument is target velocity.
targetThrust :: GameVector -> GameVector -> GameVector
targetThrust (dx, dy) (tdx, tdy) = (clampThrust $ dx - tdx, clampThrust $ dy - tdy)
 where
  clampThrust = clamp (-1) 1

thrustCommand :: GameRole -> ShipState -> Maybe Command 
thrustCommand _ SS{attr = SA{..}} | fuel <= 0 = Nothing
thrustCommand myRole SS{..} | myRole == ssRole =
  case targetThrust estNewVel tgtV of
   (0, 0) -> Nothing
   tgt -> Just $ accelerateCmd ssShipId tgt
 where
  gravEstimate = gravityEstimate position
  tgtCCW = targetVelocity gravEstimate -- rotate 90 and inscrease
  tgtCW = bimap negate negate tgtCCW -- backwards
  estNewVel = vectorAdd velocity gravEstimate
  dv1 (x1, y1) (x2, y2) = abs (x2 - x1) + abs (y2 - y1)
  dv2 (x1, y1) (x2, y2) = max (abs $ x2 - x1) (abs $ y2 - y1)
  tgtV =
    case compare (dv1 estNewVel tgtCCW) (dv1 estNewVel tgtCW) of
     LT -> tgtCCW
     EQ -> bool tgtCW tgtCCW $ dv2 estNewVel tgtCCW < dv2 estNewVel tgtCW
     GT -> tgtCW
thrustCommand _ _ = Nothing

thrustCommands :: GameRole -> [ShipState] -> [Command]
thrustCommands myRole = mapMaybe $ thrustCommand myRole
