module Junkfood.Cmdline (jfParserInfo, JFOptions(..)) where

-- base
import Control.Applicative ((<**>), optional)

-- optprase-applicative
import Options.Applicative (
  Parser, ParserInfo,
  argument, strArgument, strOption, helper, help, metavar, short, long,
  info, fullDesc, header, progDesc, footer, failureCode,
  auto,
  )

-- | Translation of any valid command-line
data JFOptions = JFO
  { serverUrl :: String -- ^ Server URL
  , playerKey :: Integer -- ^ Player Key
  , apiKey :: Maybe String -- ^ Team API Key
  } deriving (Eq, Ord, Show, Read)

-- | Parse two string arguments
jfoParser :: Parser JFOptions
jfoParser = JFO <$> serverUrlParser <*> playerKeyParser <*> apiKeyParser
 where
  serverUrlParser = strArgument
    (  help "Server URL"
    <> metavar "serverUrl"
    )
  playerKeyParser = argument auto
    (  help "Player Key"
    <> metavar "playerKey"
    )
  apiKeyParser = optional $ strOption
    (  short 'k'
    <> long "api-key"
    <> help "Team API Key"
    <> metavar "apiKey"
    )
  {-
  Note: Do not use `short 'h'` or `long "help"`, since they are used by `helper` which is added
  later.
  -}

-- | Parse command line or print brief program description
jfParserInfo :: ParserInfo JFOptions
jfParserInfo =
  -- Note: jfoParser should avoid `short 'h'` or `long "help"` since `helper` wants them.
  info (jfoParser <**> helper)
    (  fullDesc
    <> header "ICFP Programming Contest 2020 Entry"
    <> progDesc "Team JunkFood submission for the ICFP Programming Contest 2020 powered by Haskell"
    <> footer "Gitlab: https://gitlab.com/krakrjak/icfp2020-junkfood"
    <> failureCode 1
    )
