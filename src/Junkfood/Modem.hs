{-# language BangPatterns #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
module Junkfood.Modem (
  RadioData(..), pattern RDList, rdList, rdShowPretty,
  Signal, modulate, signalBitString, demodulate, demodulatePrefix, bitStringSignal
  )
where

-- base
import Data.Bits (Bits(unsafeShiftL, testBit, bit, setBit))
import Data.Bool (bool)
import Data.Char (ord)
import Data.List (intercalate)
import GHC.Generics

-- aesom
import Data.Aeson (ToJSON, FromJSON)

-- text
import Data.Text (Text, pack)

data RadioData
  = RDNil
  | RDI Integer
  | RDPair RadioData RadioData
  deriving (Eq, Show, Read, Generic)

instance ToJSON RadioData where
instance FromJSON RadioData where

pattern RDList :: [RadioData] -> RadioData
pattern RDList l <- (rdList -> Right l)
 where
  RDList []     = RDNil
  RDList (h:t)  = RDPair h $ RDList t

rdList :: RadioData -> Either Text [RadioData]
rdList RDNil = Right []
rdList (RDI i) = Left $ "rdList: expected list, got integer: " <> pack (show i)
rdList (RDPair h t) = (h:) <$> rdList t

-- | 'show' alternative that does not match 'read', but look prettier.
rdShowPretty :: RadioData -> String
rdShowPretty (RDList l) = "( " ++ intercalate " , " (map rdShowPretty l) ++ " )"
rdShowPretty (RDI i) = show i
rdShowPretty rd = show rd

type Signal = [Bool]

(.<<.) :: Bits a => a -> Int -> a
(.<<.) = unsafeShiftL

{-|
Modulate a value, preparing it to be sent via binary radio signal.

Doctests:

>>> signalBitString $ modulate (RDI 0)
"010"
>>> signalBitString $ modulate (RDI 1)
"01100001"
>>> signalBitString $ modulate (RDI (-1))
"10100001"
>>> signalBitString $ modulate (RDI 2)
"01100010"
>>> signalBitString $ modulate (RDI (-2))
"10100010"
>>> signalBitString $ modulate (RDI 16)
"0111000010000"
>>> signalBitString $ modulate (RDI (-16))
"1011000010000"
>>> signalBitString $ modulate (RDI 255)
"0111011111111"
>>> signalBitString $ modulate (RDI (-255))
"1011011111111"
>>> signalBitString $ modulate (RDI 256)
"011110000100000000"
>>> signalBitString $ modulate (RDI (-256))
"101110000100000000"

>>> signalBitString $ modulate RDNil
"00"
>>> signalBitString $ modulate (RDPair RDNil RDNil)
"110000"
>>> signalBitString $ modulate (RDPair (RDI 0) RDNil)
"1101000"
>>> signalBitString $ modulate (RDPair (RDI 1) (RDI 2))
"110110000101100010"
>>> signalBitString $ modulate (RDPair (RDI 1) (RDPair (RDI 2) RDNil))
"1101100001110110001000"

>>> signalBitString . modulate $ RDList [RDI 1, RDI 2]
"1101100001110110001000"
>>> signalBitString . modulate $ RDList [RDI 1, RDList [RDI 2, RDI 3], RDI 4]
"1101100001111101100010110110001100110110010000"
-}
modulate :: RadioData -> Signal
modulate RDNil = [False, False]
modulate (RDI i) =
  if i < 0
   then [True, False] ++ modulateNN (abs i)
   else [False, True] ++ modulateNN i
 where
  modulateNN n = go 0
   where
    go !b =
      if n < bit b
       then False : map (testBit n) [(b-1),(b-2)..0]
       else True : go (b + 4)
modulate (RDPair f s) = [True, True] ++ modulate f ++ modulate s

{-|
Convert a signal to a value.  If no value corresponds to the signal, no failure is indicated, and
an arbitrary value is returned.

demodulate . modulate = id
-or-
forall x. demodulate (modulate x) = x

Doctests:

>>> demodulate $ bitStringSignal "010"
RDI 0
>>> demodulate $ bitStringSignal "01100001"
RDI 1
>>> demodulate $ bitStringSignal "10100001"
RDI (-1)
>>> demodulate $ bitStringSignal "01100010"
RDI 2
>>> demodulate $ bitStringSignal "10100010"
RDI (-2)
>>> demodulate $ bitStringSignal "0111000010000"
RDI 16
>>> demodulate $ bitStringSignal "1011000010000"
RDI (-16)
>>> demodulate $ bitStringSignal "0111011111111"
RDI 255
>>> demodulate $ bitStringSignal "1011011111111"
RDI (-255)
>>> demodulate $ bitStringSignal "011110000100000000"
RDI 256
>>> demodulate $ bitStringSignal "101110000100000000"
RDI (-256)

>>> demodulate $ bitStringSignal "00"
RDNil
>>> demodulate $ bitStringSignal "110000"
RDPair RDNil RDNil
>>> demodulate $ bitStringSignal "1101000"
RDPair (RDI 0) RDNil
>>> demodulate $ bitStringSignal "110110000101100010"
RDPair (RDI 1) (RDI 2)
>>> demodulate $ bitStringSignal "1101100001110110001000"
RDPair (RDI 1) (RDPair (RDI 2) RDNil)


>>> demodulate (bitStringSignal "1101100001110110001000") == RDList [RDI 1, RDI 2]
True
>>> demodulate (bitStringSignal "1101100001111101100010110110001100110110010000") == RDList [RDI 1, RDList [RDI 2, RDI 3], RDI 4]
True
-}
demodulate :: Signal -> RadioData
demodulate = fst . demodulatePrefix

-- | Demodulate a value and report any remainder
demodulatePrefix :: Signal -> (RadioData, Signal)
demodulatePrefix (False:False:t) = (RDNil, t)
demodulatePrefix (False:True:t) = (RDI n, r)
 where (n, r) = demodulatePrefixNN t
demodulatePrefix (True:False:t) = (RDI (-n), r)
 where (n, r) = demodulatePrefixNN t
demodulatePrefix (True:True:t)= (RDPair f s, rs)
 where
  (f, rf) = demodulatePrefix t
  (s, rs) = demodulatePrefix rf
demodulatePrefix _ = (RDI 0, []) -- Bad Signal

-- | Demodulate a non-negative number (no sign prefix) and also report any remainder
demodulatePrefixNN :: Signal -> (Integer, Signal)
demodulatePrefixNN = go1 (0 :: Int)
 where
  -- go1 reads the length bits
  go1 !b (True:t) = go1 (b + 4) t
  go1 !b (False:t) = go2 b 0 t
  go1 _  [] = (0, []) -- Bad signal
  -- go2 reads the value bits
  go2 0  n t = (n, t)
  go2 !b n (h:t) = go2 (b-1) (bool n2 (setBit n2 0) h) t
   where n2 = n .<<. 1
  go2 _ _ [] = (0, []) -- Bad signal

-- | Convert "110010101" binary string to a [True,True,False,False,True,...] singal
bitStringSignal :: String -> Signal
bitStringSignal = map (toEnum . (`mod` 2) . ord)

-- | Convert [True,True,False,Flase,True,...] signal to "11001..." binary string
signalBitString :: Signal -> String
signalBitString = map (bool '0' '1')
