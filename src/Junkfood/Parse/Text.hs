{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}

module Junkfood.Parse.Text where

-- base
import Control.Monad (zipWithM)

import Junkfood.Parse.Text.Type
import Junkfood.Parse.Text.Attoparsec
import Junkfood.Untyped.Expr (lispExprToExpr)

import Data.Bifunctor

import Data.Aeson

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T

import qualified Data.Attoparsec.Text as A

galaxyTxt :: IO Text
galaxyTxt = T.readFile "reference/galaxy.txt"

galaxyLines :: IO [Text]
galaxyLines = T.lines <$> galaxyTxt

-- | This is the final line, the definition of "galaxy"
exceptionalGalaxyLine :: Text
exceptionalGalaxyLine = "galaxy = :1338"


-- | Asserts that `exceptionalGalaxyLine` is the last line and drops it
ignoreExceptionalGalaxyLine :: [Text] -> [Text]
ignoreExceptionalGalaxyLine [] = error "No exceptionalGalaxyLine found"
ignoreExceptionalGalaxyLine [x]
  | x == exceptionalGalaxyLine = []
  | otherwise = error "No exceptionalGalaxyLine found"
ignoreExceptionalGalaxyLine (x:xs) =
  x : ignoreExceptionalGalaxyLine xs

parsedGalaxyLines :: IO [RawDecl]
parsedGalaxyLines = do
  parsedGalaxyLines' <-
    zipWithM (\i -> first ((show i ++ ":") ++) . A.parseOnly rawDecl)
    [(1::Int)..] .
    ignoreExceptionalGalaxyLine <$>
    galaxyLines
  either
    (fail . ("parsedGalaxyLines: parsing failed: " ++) . show)
    return
    parsedGalaxyLines'

galaxyTokens :: IO AllTokens
galaxyTokens = foldMap rawDeclToAllTokens <$> parsedGalaxyLines

galaxyDecls :: IO (Either String [LispDecl])
galaxyDecls =
  mapM (A.parseOnly lispDecl) . ignoreExceptionalGalaxyLine <$>
  galaxyLines

untypedExprHandlesGalaxy :: IO (Either String ())
untypedExprHandlesGalaxy = (>>= mapM_ (lispExprToExpr . lispDeclBody)) <$> galaxyDecls

-- | Export the parsed lines and sets of tokens
exportParsedGalaxy :: IO ()
exportParsedGalaxy = do
  encodeFile "reference/parsed_galaxy.json" =<< parsedGalaxyLines
  encodeFile "reference/galaxy_tokens.json" =<< galaxyTokens
  encodeFile "reference/lisp_galaxy.json" =<< galaxyDecls

