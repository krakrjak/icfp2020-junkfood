{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Junkfood.Parse.Text.Type where

import GHC.Generics

import Data.Aeson

import Data.Text (Text)

import Data.Set (Set)

newtype Symbol = Symbol
  { unSymbol :: Int
  }
  deriving (Eq, Ord, Read, Show, Generic)

instance ToJSON Symbol where toEncoding = genericToEncoding defaultOptions
instance FromJSON Symbol



data Token
  = TLit !Integer
  | TSym !Symbol
  | TVar !Text
  deriving (Eq, Ord, Read, Show, Generic)

instance ToJSON Token where toEncoding = genericToEncoding defaultOptions
instance FromJSON Token

apToken :: Token
apToken = TVar "ap"

data RawDecl = RawDecl
  { rawDeclHead :: Symbol
  , rawDeclBody :: [Token]
  }
  deriving (Eq, Ord, Read, Show, Generic)

instance ToJSON RawDecl where toEncoding = genericToEncoding defaultOptions
instance FromJSON RawDecl


data AllTokens = AllTokens
  { allLits :: Set Integer
  , allSyms :: Set Symbol
  , allVars :: Set Text
  }
  deriving (Eq, Ord, Read, Show, Generic)

instance ToJSON AllTokens where toEncoding = genericToEncoding defaultOptions
instance FromJSON AllTokens

instance Semigroup AllTokens where
  (<>) (AllTokens a b c) (AllTokens x y z) =
    AllTokens (a <> x) (b <> y) (c <> z)

instance Monoid AllTokens where
  mempty = AllTokens [] [] []

tokenToAllTokens :: Token -> AllTokens
tokenToAllTokens =
  \case
    TLit x -> AllTokens [x] [] []
    TSym x -> AllTokens [] [x] []
    TVar x -> AllTokens [] [] [x]

rawDeclToAllTokens :: RawDecl -> AllTokens
rawDeclToAllTokens RawDecl{..} =
  tokenToAllTokens (TSym rawDeclHead) <>
  foldMap tokenToAllTokens rawDeclBody

data LispExpr
  = Ap LispExpr LispExpr
  | Tok Token
  deriving (Eq, Ord, Read, Show, Generic)

instance ToJSON LispExpr where toEncoding = genericToEncoding defaultOptions
instance FromJSON LispExpr

data LispDecl = LispDecl
  { lispDeclHead :: Symbol
  , lispDeclBody :: LispExpr
  }
  deriving (Eq, Ord, Read, Show, Generic)

instance ToJSON LispDecl where toEncoding = genericToEncoding defaultOptions
instance FromJSON LispDecl

