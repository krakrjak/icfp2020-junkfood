module Junkfood.Parse.Text.Attoparsec where

import Junkfood.Parse.Text.Type

import Control.Applicative
import Data.Char
import Data.Functor

import Data.Attoparsec.Text (Parser, (<?>))
import qualified Data.Attoparsec.Text as A

space :: Parser ()
space = void (A.char ' ') <?> "space"

eq :: Parser ()
eq = void (A.char '=') <?> "eq"

number :: Parser Integer
number = positive <|> negative
  where
    positive = A.decimal
    negative = A.char '-' >> fmap negate positive

symbol :: Parser Symbol
symbol = A.char ':' *> (Symbol <$> A.decimal)

token :: Parser Token
token = (lit <|> sym <|> var) <?> "token"
  where
    lit = TLit <$> number <?> "lit"
    sym = TSym <$> symbol <?> "sym"
    var = TVar <$> A.takeWhile1 isLetter <?> "var"

rawDecl :: Parser RawDecl
rawDecl = do
  rawDeclHead' <- symbol
  space >> eq >> space
  rawDeclBody' <- token `A.sepBy1` space
  return $ RawDecl
    rawDeclHead'
    rawDeclBody'

lispExpr :: Parser LispExpr
lispExpr = do
  token' <- token
  A.skipSpace
  if token' == apToken
     then Ap <$> lispExpr <*> lispExpr
     else return $ Tok token'

lispDecl :: Parser LispDecl
lispDecl = do
  lispDeclHead' <- symbol
  space >> eq >> space
  LispDecl lispDeclHead' <$> lispExpr

